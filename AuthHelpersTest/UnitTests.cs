using AppConnectAuth;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace AuthHelpersTest
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void GetS3LinkTest()
        {
            // setup
            S3Request g = new S3Request()
            {
                AccessKey = @"GOOG1ENU4FVMMJSODMAIB5BEHKDPRLKSWOVMS6IHUZDYQEAFKMPYIH3QCX2HI",
                Bucket = @"pubsite_prod_rev_09967845925945868084",
                ExpiresInMin = 1,
                FullFileName = @"stats/installs/installs_com.mathletics_202104_overview.csv",
                Secret = @"B/w94E6NAjDh04fQVreeEJxFS57DHTYUEcjzdj3H",
                ServiceUrl = @"https://storage.googleapis.com"
            };
            //act
            string link = g.GetLink();
            //Assert
            Assert.AreEqual("https", link.Substring(0, 5), "Link could not be generated");
        }
    }
}
