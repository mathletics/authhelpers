using Jose;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace AppConnectAuth
{
    public static class AppConnectAuth
    {
        [FunctionName("AppConnectAuth")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
            ILogger log)
        {
            string issuerId = Environment.GetEnvironmentVariable("ISSUER_ID");
            string keyId = Environment.GetEnvironmentVariable("KEY_ID");
            string secret = WebUtility.UrlDecode(Environment.GetEnvironmentVariable("KEY"));
            string authorization = req.Headers["Authorization"];
            if (authorization == null || !authorization.Equals("Bearer NFN4N3lPcmhvMTFqMkVRTzlzenhRSFFSclR4RzVOCg=="))
            {
                return new StatusCodeResult(403);
            }
            long expTime = DateTimeOffset.Now.AddMinutes(2).ToUnixTimeSeconds();
            Dictionary<string, object> headers = new Dictionary<string, object>
            {
                {"alg", "ES256"},
                {"kid", keyId},
                {"typ", "JWT"}
            };
            Dictionary<string, object> payload = new Dictionary<string, object>()
            {
                { "iss", issuerId },
                { "exp", expTime },
                { "aud", "appstoreconnect-v1" }
            };

            CngKey key = CngKey.Import(Convert.FromBase64String(secret), CngKeyBlobFormat.Pkcs8PrivateBlob);
            string token = JWT.Encode(payload, key, JwsAlgorithm.ES256, headers);
            log.LogInformation("AppConnectAuth function processed a request.");
            return new OkObjectResult("{\"token\":\"" + token + "\"}");
        }
    }
}
