using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;

namespace AppConnectAuth
{
    public static class GetS3Link
    {
        [FunctionName("GetS3Link")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("GetS3Link function processed a request.");

            string authorization = req.Headers["Authorization"];
            if (authorization == null || !authorization.Equals("Bearer MGNaOE0zRzZKY2ttdlhXcEZ2VHh2TDlHeG96Wno4Cg=="))
            {
                return new StatusCodeResult(403);
            }

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            S3Request request = JsonConvert.DeserializeObject<S3Request>(requestBody);
            return new OkObjectResult("{\"Response\":\"" + request.GetLink() + "\"}");
        }
    }
    public class S3Request
    {
        public string ServiceUrl { get; set; }
        public string Bucket { get; set; }
        public string FullFileName { get; set; }
        public string AccessKey { get; set; }
        public string Secret { get; set; }
        public int ExpiresInMin { get; set; }
        public string GetLink()
        {
            AmazonS3Config config = new AmazonS3Config { ServiceURL = ServiceUrl };
            using AmazonS3Client client = new AmazonS3Client(AccessKey, Secret, config);
            GetPreSignedUrlRequest pr = new GetPreSignedUrlRequest()
            {
                BucketName = Bucket,
                Key = FullFileName,
                Expires = DateTime.Now.AddMinutes(ExpiresInMin)
            };
            return client.GetPreSignedURL(pr);
        }
    }
}
